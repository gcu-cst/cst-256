<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Network</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link href="resources/css/app.css" rel="stylesheet">
    <!-- Scripts -->
    <script src="resources/js/app.js" defer></script>

</head>
<body class="container">
<div class="login center largeMarginTop spaceAfter">
    <div class="logo">
        <span>
            <i class="fa fa-plug" aria-hidden="true"></i>
        </span>
        <p>PLUGGED</p><p style="color: #0275d8">IN</p>
    </div>

    <div class="row spaceAfter">
        <div class="btn-group center" role="group" aria-label="Login">
            <button id="loginButton" type="button" class="btn btn-primary largeButton" onclick="toggleClass('login')">Login</button>
            <button id="registerButton" type="button" class="btn btn-secondary largeButton" onclick="toggleClass('register')">Register</button>
        </div>
    </div>

    <!-- The following 2 forms are controlled by javascript to show upon toggle button select -->

    <form id="loginForm" method="post" action="loginHandler.php">
        <div>
            <div class="row spaceAfter center">
                <label for="username" class="spaceRight lightText largeButton">Username: </label>
                <input type="text" id="username" name="username" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row spaceAfter center">
                <label for="password" class="spaceRight lightText largeButton">Password: </label>
                <input type="password" id="password" name="password" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row">
                <input type="submit" value="Submit" class="btn btn-primary largeButton center"/>
            </div>
        </div>
    </form>

    <form id="registrationForm" method="post" action="registerHandler.php">
        <div>
            <div class="row spaceAfter center">
                <label for="firstName" class="spaceRight lightText largeButton">First Name: </label>
                <input type="text" id="firstName" name="firstName" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row spaceAfter center">
                <label for="lastName" class="spaceRight lightText largeButton">Last Name: </label>
                <input type="text" id="lastName" name="lastName" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row spaceAfter center">
                <label for="email" class="spaceRight lightText largeButton">Email: </label>
                <input type="text" id="email" name="email" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row spaceAfter center">
                <label for="screenName" class="spaceRight lightText largeButton">Username: </label>
                <input type="text" id="screenName" name="screenName" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row spaceAfter center">
                <label for="pass" class="spaceRight lightText largeButton">Password: </label>
                <input type="text" id="pass" name="pass" class="form-control spaceRight loginFormControl"/>
            </div>
            <div class="row">
                <input type="submit" value="Register" class="btn btn-primary largeButton center"/>
            </div>
        </div>
    </form>
</div>
</body>
</html>
