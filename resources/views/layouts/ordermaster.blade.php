<html lang="en">
<head>
    <title>@yield('title')</title>
</head>
<body style="margin: 0; padding: 0; height: 100%; display: flex; flex-direction: column; align-items: center; justify-content: space-between; font-family: sans-serif">
@include('layouts.orderHeader')
<div style="width: 100%; height: 100%; display: flex; flex-direction: column; align-items: center; justify-content: center;">
    @yield('content')
</div>
</body>
</html>
