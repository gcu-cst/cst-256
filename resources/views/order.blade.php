@extends('layouts.ordermaster')
@section('title', 'Order Page')
@section('content')
    <div style="display: flex; flex-direction: column; align-items: center; justify-content: flex-start">
        <form action = "order/add" method = "POST">
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
            <h2>Place Order</h2>
            <table>
                <tr>
                    <td>First Name: </td>
                    <td><input type = "text" name = "firstName" /></td>
                </tr>

                <tr>
                    <td>Last Name:</td>
                    <td><input type = "text" name = "lastName" /></td>
                </tr>

                <tr>
                    <td>Product:</td>
                    <td><input type = "text" name = "product" /></td>
                </tr>

                <tr>
                    <td colspan = "2" align = "center">
                        <input type = "submit" value = "PLACE ORDER" />
                    </td>
            </table>
        </form>
    </div>
@endsection
