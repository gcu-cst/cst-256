<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <form action = "dologin" method = "POST">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token() ?>">
        <h2>Avengers Login</h2>
        <table>
            <tr>
                <td>Username: </td>
                <td><input type = "text" name = "username" /></td>
            </tr>

            <tr>
                <td>Password:</td>
                <td><input type = "password" name = "password" /></td>
            </tr>
            <tr>
                <td colspan = "2" align = "center">
                    <input type = "submit" value = "Login" />
                </td>
        </table>
    </form>
</body>
</html>
