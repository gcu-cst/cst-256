<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(): string {
        return "Hello World";
    }

    public function test2() {
        return view('helloworld');
    }
}
