<?php

namespace App\Http\Controllers;

use App\Services\Business\OrderService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request) {
        $orderService = new OrderService();

        $firstName = $request->input('firstName');
        $lastName = $request->input('lastName');
        $product = $request->input('product');

        $orderService->createOrder($firstName, $lastName, $product);
    }
}
