<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Illuminate\Http\Request;

class Login3Controller extends Controller
{
    public function index(Request $request) {
        $this->validateForm($request);
        $user = new UserModel($request->input('username'), $request->input('password'));
        if(SecurityService::login($user)) {
            return view('loginPassed2')->with('user', $user)->with('passed', 'PASSED');
        } else {
            return view('loginPassed2')->with('user', $user)->with('passed', 'FAILED');
        }
    }

    private function validateForm(Request $request) {
        // Setup Data Validation Rules for Login Form
        $rules = ['username' => 'Required | Between:4,10 | Alpha', 'password' => 'Required | Between:4,10'];

        // Run Data Validation Rules
        $this->validate($request, $rules);
    }

}
