<?php

namespace App\Http\Controllers;

use App\Models\UserModel;
use App\Services\Business\SecurityService;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(Request $request) {
        $user = new UserModel($request->input('username'), $request->input('password'));
        if(SecurityService::login($user)) {
            return view('loginPassed2')->with('user', $user)->with('passed', 'PASSED');
        } else {
            return view('loginPassed2')->with('user', $user)->with('passed', 'FAILED');
        }
    }
}
