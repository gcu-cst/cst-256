<?php


namespace App\Services\Business;

use App\Services\Data\CustomerDAO;
use App\Services\Data\OrderDAO;
use mysqli;

class OrderService {

    private function connectDB() {
        $servername = "127.0.0.1:8889";
        $dbuser = "root";
        $dbpassword = "root";
        $dbname = "activity2";

        $conn = new mysqli($servername, $dbuser, $dbpassword, $dbname);

        if($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        return $conn;
    }

    public function createOrder($firstName, $lastName, $product) {
        $conn = $this->connectDB();

        $conn->autocommit(FALSE);
        $conn->begin_transaction();

        $orderDAO = new OrderDAO($conn);
        $customerDAO = new CustomerDAO($conn);

        $customer = $customerDAO->addCustomer($firstName, $lastName);
        $orderDAO->addOrder($product, $customer['ID']);

        //$conn->commit();
        $conn->rollback();

        $conn->close();
    }

    public function testOrder($firstName, $lastName, $product) {
        $conn = $this->connectDB();

        $orderDAO = new OrderDAO($conn);
        $customerDAO = new CustomerDAO($conn);

        $customer = $customerDAO->addCustomer($firstName, $lastName);
        $orderDAO->addOrder($product, $customer['ID']);

        $conn->close();
    }
}
