<?php


namespace App\Services\Business;

use App\Models\UserModel;
use App\Services\Data\SecurityDAO;

class SecurityService {
    public static function login(UserModel $user): bool {
        return SecurityDAO::findByUser($user);
    }
}
