<?php

namespace App\Services\Data;

use App\Models\UserModel;
use Illuminate\Support\Facades\DB;

class SecurityDAO {
    public static function findByUser(UserModel $user): bool {
        if(DB::select('SELECT * FROM users WHERE USERNAME = ? AND PASSWORD = ?', [$user->getUsername(), $user->getPassword()])) {
            return true;
        } else {
            return false;
        }
    }
}

