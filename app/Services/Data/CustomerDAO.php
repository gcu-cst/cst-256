<?php


namespace App\Services\Data;


class CustomerDAO {
    public $conn;

    function __construct($conn) {
        $this->conn = $conn;
    }

    public function addCustomer($firstName, $lastName) {
        $insertSql = "INSERT INTO customers (FIRST_NAME, LAST_NAME) VALUES ('$firstName', '$lastName')";

        if ($this->conn->query($insertSql) === TRUE) {
            $selectSql = "SELECT * FROM customers WHERE FIRST_NAME = '$firstName' AND LAST_NAME = '$lastName'";
            $resultCustomer = mysqli_query($this->conn, $selectSql) or die("Bad Query: $selectSql");
            $rowCustomer = mysqli_fetch_assoc($resultCustomer);

            return $rowCustomer;
        } else {
            return null;
        }
    }
}
